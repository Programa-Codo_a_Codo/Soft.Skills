# **Presentación del curso**

## **Índice del curso**

- Módulo 0: Presentación del curso
- Módulo 1: Autoconfianza
- Módulo 2: Comunicación
- Módulo 3: Adaptabilidad
- Módulo 4: Trabajo en Equipo
- Módulo 5: Habilidades para encontrar Empleo

¡Bienvenido al Curso de Habilidades Blandas!

Desarrolla habilidades esenciales para destacar en el mundo laboral con nuestro curso autoasistido. Aprende comunicación, trabajo en equipo, resolución de problemas y más, a tu propio ritmo. Prepárate para el éxito profesional y personal. ¡Comencemos juntos este emocionante viaje!

Este curso te brinda la oportunidad de desarrollar habilidades fundamentales para el éxito en el ámbito laboral y personal. A lo largo de cinco módulos, abordaremos aspectos esenciales que te permitirán destacar en cualquier entorno profesional.

En el Módulo de presentación del curso, exploraremos una visión general de las habilidades blandas y su relevancia en el entorno laboral actual.

En el Módulo 1, examinaremos la Autoconfianza, fortaleciendo la seguridad en ti mismo y en tus habilidades para tomar decisiones con determinación.

En el Módulo 2, nos enfocaremos en la Comunicación Efectiva, tanto para expresar tus ideas de manera clara y concisa, como para escuchar y comprender las ideas de los demás.

El Módulo 3 se centra en la Adaptabilidad, brindándote las herramientas para enfrentar los cambios y encontrar soluciones creativas a los desafíos que se presenten en tu camino.

En el Módulo 4, aprenderemos sobre el Trabajo en Equipo, desarrollando habilidades para colaborar eficientemente y resolver conflictos de manera constructiva.

Finalmente, en el Módulo 5, nos enfocaremos en las Habilidades para encontrar Empleo, donde aprenderás estrategias para potenciar tu búsqueda laboral y destacar en el proceso de selección.

¡Sumate a este curso y descubrí cómo potenciar tus habilidades blandas para alcanzar tus metas profesionales y personales con confianza y éxito!

## **Objetivos**

1. Comprender el concepto de habilidades blandas (soft skills) y distinguirlas de las habilidades técnicas (hard skills).
2. Reconocer la importancia de las habilidades blandas en el desarrollo personal, laboral y social, comprendiendo su impacto significativo en el éxito profesional.
3. Identificar y desarrollar habilidades interpersonales clave, incluyendo la autoconfianza, la comunicación efectiva, la adaptabilidad, el trabajo en equipo y las habilidades para encontrar empleo.
4. Aplicar la inteligencia emocional para comprender y gestionar nuestras emociones, así como las emociones de los demás, con el objetivo de mejorar las relaciones interpersonales y la colaboración en entornos laborales.
5. Fortalecer la capacidad para resolver problemas de manera creativa y enfrentar desafíos con una mentalidad abierta y constructiva.
6. Adquirir habilidades de comunicación efectiva para expresar ideas de manera clara y concisa, así como para escuchar y comprender las ideas de los demás.
7. Potenciar el trabajo en equipo, fomentando la colaboración efectiva y la resolución constructiva de conflictos.
8. Desarrollar habilidades de adaptabilidad para enfrentar cambios y situaciones desafiantes con flexibilidad y resiliencia.
9. Mejorar la autoconfianza y la creencia en las propias habilidades para tomar decisiones con seguridad y determinación.
10. Aplicar estrategias para encontrar empleo de manera efectiva, optimizando la búsqueda laboral y destacando en el proceso de selección.

Al finalizar el curso, los participantes habrán desarrollado un conjunto integral de habilidades blandas que les permitirá destacar en el ámbito laboral y personal. Estarán preparados para enfrentar los desafíos del mundo laboral actual, mejorar la colaboración en equipos de trabajo, adaptarse a los cambios y comunicarse de manera efectiva. Además, habrán adquirido herramientas para encontrar empleo de manera más eficiente y potenciar su desarrollo profesional y personal en diversos campos.

## **Navegabilidad del curso**

El curso, en su totalidad, es autoasistido. Es decir, vas a poder realizarlo a tu ritmo y cuando quieras. Para que puedas llevar adelante este proceso de la mejor manera, te dejamos una guía de los pasos para transitar cada uno de los módulos.

1. Cuando ingreses al aula virtual, encontrarás el “Módulo 0” habilitado para la interacción. El resto, aún estará desactivado ya que todo tiene un proceso estructurado. ¡Tranquilos! Vayamos paso a paso.
2. Una vez que ingreses al primer módulo (Módulo 0), tendrás a disposición información escrita y luego, una actividad de foro.
3. Para continuar la navegación, deberás realizar la actividad del foro. Una vez ejecutada esta acción, volverás a la página principal del contenido del curso.
4. Verás que en el Módulo 0, ahora aparece un progreso del 50%. ¡Excelente! Eso significa que ya tenés la primera mitad del módulo realizado.
5. Ingresarás nuevamente al Módulo 0. Allí, encontrarás que aparece nuevo contenido para que logres finalizarlo.
6. El contenido nuevo consta de una presentación con información valiosa para recorrer y apropiarse de los temas del módulo y un foro de cierre.
7. Una vez trabajado el contenido teórico, encontrarás la última actividad del foro. Al realizarla, finalizarás el módulo.
8. Cuando vuelvas a la página principal, observarás que el Módulo 0 ahora presenta un tilde verde en señal de finalización y el módulo 1 aparece desbloqueado.
9. Comienza el siguiente módulo (Módulo 1) y sigue las mismas indicaciones en cada uno de ellos.
