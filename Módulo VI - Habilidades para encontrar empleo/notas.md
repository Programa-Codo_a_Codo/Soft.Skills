# **Habilidades para encontrar empleo**

## **Objetivos del Módulo:**

- Comprender la importancia de desarrollar habilidades específicas para la búsqueda efectiva de empleo.
- Identificar las estrategias y técnicas clave para destacar en el proceso de búsqueda laboral.
- Desarrollar habilidades de redacción y diseño de currículum vitae y cartas de presentación impactantes.
- Mejorar la capacidad para prepararse y enfrentar entrevistas de trabajo con confianza y seguridad.
- Aplicar estrategias para aprovechar las oportunidades de networking y construir una sólida red de contactos profesionales.
