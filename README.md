# **Soft Skills**

Las habilidades blandas, del inglés soft skills, comprenden una serie de competencias sociales que facilitan a las personas la relación con sus semejantes. Aunque están muy ligadas a la propia personalidad, pueden mejorarse y, pese a su carácter intangible, son muy valoradas por las empresas en la actualidad. Entre ellas están la inteligencia emocional, la gestión del cambio o el pensamiento crítico.
